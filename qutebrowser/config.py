import platform

distro = platform.dist()
if distro[0] == 'gentoo':
    c.fonts.monospace = "t cherry"
else:
    c.fonts.monospace = "cherry"


c.tabs.show = "multiple"
c.statusbar.hide =  True
c.url.start_pages = ["https://gentoo.org/"]

config.bind(',v', 'spawn mpv {url}')
