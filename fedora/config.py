c.tabs.show = "multiple"
c.statusbar.hide =  True
c.url.start_pages = ["https://start.fedoraproject.org/en/"]
c.fonts.monospace = "cherry"
config.bind(',v', 'spawn mpv {url}')
